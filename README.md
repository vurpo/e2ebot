# e2ebot

A simple ping bot that supports end-to-end encryption.

Use `login.py` to login to a Matrix account. Then, run `main.py`.

Invite the user to a room and say `!ping` to see the bot respond.
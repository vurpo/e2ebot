#!/usr/bin/env python3
""" 
from matrix_client.client import MatrixClient
from matrix_client.crypto.olm_device import OlmDevice

client = MatrixClient(config['homeserver'], encryption=True)#, token=config['token'], user_id=config['user_id'], restore_device_id=True, device_id=config['device_id'])

# work around a shortcoming in the E2E support, can't automatically create a MatrixClient with an access token
client.user_id = config['user_id']
client.token = config['token']
client.api.token = client.token
client.device_id = config['device_id']

if not client.olm_device:
    client.olm_device = OlmDevice(
        client.api, client.user_id, client.device_id, **client.encryption_conf)
client.olm_device.upload_identity_keys()
client.olm_device.upload_one_time_keys()

client.sync_filter = '{ "room": { "timeline" : { "limit" : 10 } } }'
client._sync()
# done loading

client.verify_devices = False

print(client.get_rooms())

def event_listener(room, event):
    
def invite_listener(room_id, state):
    client.join_room(room_id)
    client.get_rooms()[room_id].add_listener(event_listener)

client.add_invite_listener(invite_listener)

for id, room in client.get_rooms().items():
    room.add_listener(event_listener)

client.listen_forever() """

import asyncio
from nio import (AsyncClient, RoomMessageText, InviteMemberEvent, MatrixInvitedRoom, MatrixRoom)
import json

with open("config.json", "r") as f:
    config = json.load(f)

class Bot():
    def __init__(self):
        self.client = None
    
    async def login(self, config):
        self.client = AsyncClient(config['homeserver'], config['user_id'], config['device_id'])
        self.client.add_event_callback(self.invite_listener, InviteMemberEvent)
        self.client.add_event_callback(self.event_listener, RoomMessageText)
        await self.client.login(config['password'])
    
    async def run_forever(self):
        await self.client.sync_forever(timeout=30000)

    async def event_listener(self, room: MatrixRoom, event: RoomMessageText):
        if event.sender != self.client.user_id:
            if event.body == "!leave" and event.sender == config['owner']:
                await client.room_leave(room.room_id)
                return
            elif event.body == "!ping":
                print("Ping!")
                await self.client.room_send(
                    room_id=room.room_id,
                    message_type="m.room.message",
                    content={
                        "msgtype":"m.notice",
                        "body":"Pong!"
                    }
                )
            print(
                "Message received for room {} | {}: {}".format(
                    room.display_name, room.user_name(event.sender), event.body
                )
            )

    async def invite_listener(self, room, event):
        if isinstance(room, MatrixInvitedRoom):
            await self.client.join(room.room_id)

async def main():
    bot = Bot()
    await bot.login(config)
    await bot.run_forever()



asyncio.get_event_loop().run_until_complete(main())